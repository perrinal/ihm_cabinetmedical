Fait par MENDES Julien & PERRIN Alexandre
## WEB APPLICATION CABINET MEDICAL
# Etapes pour pouvoir tester, dev  le projet 
	- git clone https://gricad-gitlab.univ-grenoble-alpes.fr/perrinal/ihm_cabinetmedical.git
	- cd ihm_cabinetmedical
	- cd clientAngular
	- npm install
	- cd ../serveurAngular 
	- npm install
	Maintenant on lance le serveur et client : 
	- Dans /clientAngular : npm start
	- Dans /serveurAngular : npm start 
	- web client: "localhost:4200" 
	- localhost:8090 ne permet pas d'acceder a notre page secretaire
# Fonctionnalités
## Secretary :
	- Barre d'outils : ( routes ) 
		- Home : Cliquer sur image cabinet
		- Infirmiers : bouton
		- Patients : bouton
	- <router-outlet>
	- Pied de page 
## Home
	Info générale sur le cabinet ( nb patients, nb infirmiers ) & ( nb de patients affectés, nb non affectés)
## Infirmiers :
	Liste des infirmiers
		- clickable pour dérouler les patients qui lui sont affectés
	Liste des patients affecté à l'infirmier
		- affiche un message d'avertissement si aucun patient affecté
		- sinon affiche les données de chaques patients sous forme d'un tableau
		- Un bouton désaffecté est présent pour chaque patient 
## Patients :
	Liste des patients :
		- Trie en cliquant sur le nom de la colonne ( asc , dsc )
	Filtres patients : 
		- Non affecté, affecté, tous
		- Input pour recherche précise : nom , n°secu ... 
	Ajouter un patient ( dialog s'ouvre: dialog-form-patient) :
		- Validators pattern + required appliqué a chaque champ 
		- Et pour "numéro sécurité sociale" : Un message d'erreur s'affiche si le numéro est déja attribué 
		-> Tout ce qui est saisie par l'utilisateurs doit etre conforme, bien formé sinon un message d'erreur apparait 
		Bouton "ajouter" bloqué tant que un des champs est non conforme
	Editer un patient ( dialog s'ouvre: dialog-form-patient) : Si un des champs n'est pas conforme avec son REGEX, le bouton pour mettre a jour le patient est bloqué
	Affecter un patient ( dialog s'ouvre: dialog-affectation-patient ) :
		- Aprés avoir selectionner un patient, on peut le désaffecter ou bien l'affecter 
	Bouton "Affectations": Gestion des affectations : 
		- Une liste de cdkDragList s'affiche : 
			- une pour les patients non affectés 
			- toute les autres correspond aux patients affecté à un infirmier (pour chaques infirmier, on crée une cdkDropList)
		-> On affecte/désaffecte seulement les patients qui ont été drag drop dans ce dialogue
	Bulle d'info lors qu'on survol un bouton :  matToolTip
	Petite fênetre s'ouvre en bas de l'écrans lors de la réaisation de certaines actions : snackBar 
	Si aucun patient n'est séléctionné alors un message est affiché en fonction
							

## Problèmes :
	- post : /addPatients 
		-> Efface les intervenants et actes du patients 
	Comme faut pas toucher le serveur je comprends pas
	- bug cdkDragList avec dialog : [mat-dialog-close] est call a chaque fois qu'on clique sur un cdkDragElement 
		-> Impossible de renvoyer une valeur au parent aprés la fermeture du dialog
	- Fuite mémoire ? : A chaque fois qu'on refresh la page, le processus web devient de plus en plus lourd (de base 400Mo, aprés de multiples refresh plus de 1.8G de RAM )
		-> Relancer le serveur pour libérer toute la RAM 
