import {Adresse} from './adresse';
import {sexeEnum} from './sexe';


export interface PatientInterface {
  prenom: string;
  nom: string;
  sexe: sexeEnum;
  numeroSecuriteSociale: string;
  adresse: Adresse;
}
