import {Component, OnInit, ViewChild} from '@angular/core';
import {CabinetInterface} from '../dataInterfaces/cabinet';
import {CabinetMedicalService} from '../cabinet-medical.service';
import {PatientInterface} from '../dataInterfaces/patient';
import {
  MatDialog,
  MatDialogConfig,
  MatPaginator,
  MatSnackBar,
  MatSort,
  MatSortable,
  MatTableDataSource
} from '@angular/material';
import {DialogueFormPatientComponent} from '../dialogue-form-patient/dialogue-form-patient.component';
import {InfirmierInterface} from '../dataInterfaces/infirmier';
import {DialogueAffectationPatientComponent} from '../dialogue-affectation-patient/dialogue-affectation-patient.component';


@Component({
  selector: 'app-patients',
  templateUrl: './patients.component.html',
  styleUrls: ['./patients.component.scss']
})
export class PatientsComponent implements OnInit {
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatPaginator) paginator: MatPaginator;

  private _cms: CabinetInterface;
  private _patients: PatientInterface[];
  private _sortedPatients: MatTableDataSource<any>;

  /** Variable data binding selon la vue**/
  _isAffectationMod: boolean;
  _selectId: any;
  _filterMod: string;
  _filterValue: string;
  _selectPatient: PatientInterface;


  /** Infirmiers affectés au patient selectionné '_selectPatient' */
  infirmiersAssignedToSelectPatient: InfirmierInterface[];

  displayedColumns: string[] = ['prenom', 'nom', 'sexe', 'numeroSecuriteSociale'];

  public get cms(): CabinetInterface { return this._cms; }
  public get patients(): PatientInterface[] { return this._patients; }
  public get sortedPatients(): MatTableDataSource<any> { return this._sortedPatients; }

  constructor(public cabinetMedicalService: CabinetMedicalService, public dialog: MatDialog, public snackBar: MatSnackBar ) {
    this.initCabinet(cabinetMedicalService, false);
  }

  /** @desc Initialise le cabinent médical '_cms' */
  async initCabinet(cabinetMedicalService, afterDialogClosed: boolean) {
    this._cms = await cabinetMedicalService.getData('/data/cabinetInfirmier.xml');
    this._patients =  [...cabinetMedicalService.getPatients(this.cms)];
    this._sortedPatients = new MatTableDataSource(this._patients);

    this._sortedPatients.sort = this.sort;
    this._sortedPatients.paginator = this.paginator;

    /** Si j'enleve la condition, le trie du tableau aprés la fermeture du dialog ( affectation patients ) bug */
    if (!afterDialogClosed) {
      /** Sort by default: 'prenom' ascendant */
      this.sort.sort(<MatSortable>{
        id: 'prenom',
        start: 'asc'
      });

      this._filterMod = 'all';
      this.filterPatient();
    }

    if (afterDialogClosed) {
      this.filterPatient();
      if (this._filterValue) {
        this.applyFilter();
      }
    }

    this._isAffectationMod = false;

    if (this._selectPatient) {
      this.getAffectationsPatient();
    }

  }

  ngOnInit() {
  }

  /** @desc Bar qui s'affiche en bas de l'ecran lors d'une action du type : ajouter , update , affecter patient */
  openSnackBar(message: string, action: string) {
    this.snackBar.open(message, action, {
      duration: 5000,
    });
  }

  /** @desc Ouverture du dialogue permettant d'ajouter ou mettre à jour un patient */
  openDialog(patient: PatientInterface): void {

    const dialogConfig = new MatDialogConfig();

    dialogConfig.width = '300px';
    dialogConfig.disableClose = true;
    dialogConfig.data = patient;

    const dialogRef = this.dialog.open(DialogueFormPatientComponent, dialogConfig );

    dialogRef.afterClosed().subscribe(result => {
      if ( result !== null && result !== undefined) {
        if ( patient === null ) {
          this.addPatient(result);
          this.openSnackBar('Ajout patient n°' + result.numeroSecuriteSociale, 'OK');
        } else {
          if (this.infirmiersAssignedToSelectPatient.length === 0) {
          this.addPatient(result, true);
          } else {
            this.updatePatient(result);
          }
          this.openSnackBar('Update du patient n°' + result.numeroSecuriteSociale, 'OK');

        }
      }
    });
  }

  /** @desc Ouverture du dialogue permettant de gérer les affectations des patients */
  openDialogAffectations(): void {

    const dialogConfig = new MatDialogConfig();

    dialogConfig.disableClose = true;


    const dialogRef = this.dialog.open(DialogueAffectationPatientComponent, dialogConfig );

    dialogRef.afterClosed().subscribe((result) => {
      /** Re initialisation du cabinet */
      this.initCabinet(this.cabinetMedicalService, true);
      this._selectPatient = null;
    });
  }

  /** @desc Ajoute le patient 'patient' dans le cabinet */
  async addPatient(patient: PatientInterface, updateMod?: boolean) {
    const requestIsCorrect = await this.cabinetMedicalService.addPatient(patient);

    if ( requestIsCorrect !== null )  {
      /** Modification en local du cabinet '_cms' */
      if (updateMod) {
        this._cms.patientsNonAffectés =
          this._cms.patientsNonAffectés.map( (P) => P.numeroSecuriteSociale === patient.numeroSecuriteSociale ? patient : P);

      } else {
        this._cms.patientsNonAffectés.push(patient);
      }
      this._selectPatient = patient;
      this.getAffectationsPatient();

    } else {
      console.log('request POST.add: KO');
    }


    this.filterPatient();
    if (this._filterValue) {
      this.applyFilter();
    }
  }

  /** @desc Met à jour les données du patient 'patient'*/
  async updatePatient(patient: PatientInterface) {
    const requestAddIsCorrect = await this.cabinetMedicalService.addPatient(patient);
    const requestAffectationIsCorrect = await this.cabinetMedicalService.affectationPatient
      (this.infirmiersAssignedToSelectPatient[0].id, this._selectPatient.numeroSecuriteSociale);

    if ( requestAddIsCorrect !== null ) {
      console.log('request POST.add: OK');
      if (requestAffectationIsCorrect !== null) {
        console.log('request POST.affectation: OK');
        /** Modification en local du cabinet '_cms' */
        this._cms.patientsNonAffectés = this._cms.patientsNonAffectés
          .map( (P) => P.numeroSecuriteSociale === patient.numeroSecuriteSociale ? patient : P);
        this._cms.infirmiers.map( (I, i) => {
           this._cms.infirmiers[i].patients = I.patients
            .map((P) => P.numeroSecuriteSociale === patient.numeroSecuriteSociale ? patient : P);
        });

        this._selectPatient = patient;

        this.filterPatient();
        if (this._filterValue) {
          this.applyFilter();
        }

      } else {
        console.log('request POST.affectation: KO');
      }
    } else {
      console.log('request POST.add: KO');
    }
  }

  /** @desc Affecte le patient 'patient' a l'infirmier ayant pour id 'infirmierId' seulement si 'infirmierId' != 'none',
   *  sinon le desaffecte */
  async affectationPatient(patient: PatientInterface, infirmierId: string) {

    let requestIsCorrect;
    if ( infirmierId === 'none' ) {
      /** Désaffectation */
       requestIsCorrect = await this.cabinetMedicalService.affectationPatient('none', patient.numeroSecuriteSociale);
    } else {
      /** Affectation */
       requestIsCorrect = await this.cabinetMedicalService.affectationPatient(infirmierId, patient.numeroSecuriteSociale);
    }
    if (requestIsCorrect != null) {
      /** Modification en local du cabinet '_cms' */
      if (infirmierId === 'none') {
        this.infirmiersAssignedToSelectPatient = [];
        this._cms.infirmiers = this._cms.infirmiers
                                        .map( (infirmier) =>  {
                                          const inf = { id: infirmier.id,
                                          prenom: infirmier.prenom,
                                          nom: infirmier.nom,
                                          photo: infirmier.photo,
                                          /* Filtre les patients (n°secu) selon le patient a supprimé */
                                          patients: infirmier.patients
                                            .filter( (patient_) => patient_.numeroSecuriteSociale !== patient.numeroSecuriteSociale),
                                          adresse: infirmier.adresse
                                         };
                                          return inf;
                                        });
        this._cms.patientsNonAffectés.push(patient);
      } else {
        this.infirmiersAssignedToSelectPatient = this._cms.infirmiers.filter( (I) => I.id === infirmierId);
        this._cms.patientsNonAffectés =
          this._cms.patientsNonAffectés.filter( (P) => P.numeroSecuriteSociale !== patient.numeroSecuriteSociale);
        this._cms.infirmiers =
          this._cms.infirmiers.
            map( (infirmier) =>  {
            const inf = { id: infirmier.id,
              prenom: infirmier.prenom,
              nom: infirmier.nom,
              photo: infirmier.photo,
              /* Filtre les patients (n°secu) selon le patient a supprimé */
              patients: infirmier.patients
                .filter( (patient_) => patient_.numeroSecuriteSociale !== patient.numeroSecuriteSociale),
              adresse: infirmier.adresse
            };
            infirmier.id === infirmierId ? inf.patients.push(patient) : inf.patients.length.valueOf();
            return inf;
          });
      }
      /** Mise a jour du tableau patients selon les filtres ( non affectés , affectés, all ou input filtre )*/
      this.filterPatient();
      if (this._filterValue) {
        this.applyFilter();
      }
    } else {
      console.log('AFFECTATION REQUEST : KO', requestIsCorrect);
    }
  }

  /** @desc trouve les infirmiers assigné au '_selectPatient' et l'affecte à 'infirmiersAssignedToSelectPatient' */
  getAffectationsPatient() {
    this.infirmiersAssignedToSelectPatient = this._cms.infirmiers.filter(
      (infirmier) => infirmier.patients.filter(
      (p) => p.numeroSecuriteSociale === this._selectPatient.numeroSecuriteSociale).length !== 0);
  }

  /** @desc filtre le tableau des patients selon '_filterMod' */
  filterPatient() {
    switch (this._filterMod) {
      case 'only-affected':
        this._patients = [];
        this._cms.infirmiers.map( (I) => I.patients.map( (P) => this._patients.push(P) ));
        break;
      case 'only-not-affected':
        this._patients = [...this._cms.patientsNonAffectés];
        break;
      case 'all':
        this._patients =  [...this.cabinetMedicalService.getPatients(this.cms)];
        break;
      default :
        console.log('erreur filtre');
    }
    this._sortedPatients.data = this._patients;
  }

  /** @desc filtre le tableau des patients selon '_filterValue' */
  applyFilter() {
    this._sortedPatients.filter = this._filterValue.trim().toLowerCase();
  }






}


