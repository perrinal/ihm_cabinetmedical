import { Injectable } from '@angular/core';
import {CabinetInterface} from './dataInterfaces/cabinet';
import {HttpClient, HttpResponse} from '@angular/common/http';
import {PatientInterface} from './dataInterfaces/patient';
import {sexeEnum} from './dataInterfaces/sexe';
import {InfirmierInterface} from './dataInterfaces/infirmier';
import {Adresse} from './dataInterfaces/adresse';

@Injectable({
  providedIn: 'root'
})
export class CabinetMedicalService {

  private _cabinet: CabinetInterface;

  private _http: HttpClient;
  public get http(): HttpClient { return this._http; }

  constructor( http: HttpClient ) {
    this._http = http;
  }

  /** @desc Promet de retourner le cabinet médical ayant poursource le fichier xml a l'adresse 'url' */
  async getData( url: string ): Promise<CabinetInterface> {
    // get HTTP response as text
    const response = await this.http.get(url, { responseType: 'text' }).toPromise();

    // parse the response with DOMParser
    const parser = new DOMParser();
    const doc = parser.parseFromString(response, 'application/xml');

    // if no doc, exit
    if (!doc) { return null; }

    // default cabinet
    const cabinet: CabinetInterface = {
      infirmiers: [],
      patientsNonAffectés: [],
      adresse: this.getAdressFrom( doc.querySelector( 'cabinet' ) )
    };

    // 1 - tableau des infirmiers
    const infirmiersXML =  Array.from( doc.querySelectorAll( 'infirmiers > infirmier' ) ); // transformer la NodeList en tableau pour le map

    cabinet.infirmiers = infirmiersXML.map( I => ({
      id      : I.getAttribute('id'),
      prenom  : I.querySelector('prénom').textContent,
      nom     : I.querySelector('nom'   ).textContent,
      photo   : I.querySelector('photo' ).textContent,
      adresse : this.getAdressFrom(I),
      patients: []
    }) );

    // 2 tableau des patients
    const patientsXML  = Array.from( doc.querySelectorAll( 'patients > patient' ) );
    const patients: PatientInterface[] = patientsXML.map( P => ({
      prenom: P.querySelector('prénom').textContent,
      nom: P.querySelector('nom').textContent,
      sexe: P.querySelector('sexe').textContent === 'M' ? sexeEnum.M : sexeEnum.F,
      numeroSecuriteSociale: P.querySelector('numéro').textContent,
      adresse: this.getAdressFrom( P )
    }) );

    // 3 Tableau des affectations à faire.
    const affectations = patientsXML.map( (P, i) => {
      const visiteXML = P.querySelector( 'visite[intervenant]' );
      let infirmier: InfirmierInterface = null;

      /* Obligé de vérifier la taille de l'attribut intervenant
      /* car '/affectation' avec infirmierID: 'none' met à vide la valeur de l'attribut. */
      if (visiteXML !== null && visiteXML.getAttribute('intervenant').length !== 0) {
        infirmier = cabinet.infirmiers.find( I => I.id === visiteXML.getAttribute('intervenant') );
      }
      return {patient: patients[i], infirmier: infirmier};
    } );


    // 4 Réaliser les affectations
    affectations.forEach( ({patient: P, infirmier: I}) => {
        if (I !== null) {
          I.patients.push(P);
        } else {
          cabinet.patientsNonAffectés.push(P);
        }
      }
    );


    // Return the cabinet
    return cabinet;

  }

  /** @desc Retourne la liste des patients du cabinet médical 'cms' */
  public getPatients( cms: CabinetInterface ): PatientInterface[] {
    const patients: PatientInterface[] = [];

      cms.infirmiers.map(( p ) =>
      p.patients.map( (v) => patients.push(v)));



      cms.patientsNonAffectés.map( ( p ) => patients.push(p) );

    return patients;
  }

  /** @desc Retourne l'adresse d'un element */
  private getAdressFrom(root: Element): Adresse {
    let node: Element;
    return {
      ville       : (node = root.querySelector('adresse > ville')     ) ? node.textContent                    : '',
      codePostal  : (node = root.querySelector('adresse > codePostal')) ? parseInt(node.textContent, 10) : 0,
      rue         : (node = root.querySelector('adresse > rue')       ) ? node.textContent                    : '',
      numero      : (node = root.querySelector('adresse > numéro')    ) ? node.textContent                    : '',
      etage       : (node = root.querySelector('adresse > étage')     ) ? node.textContent                    : '',
    };
  }

  /** @desc Promet d'ajouter le patient 'patient' au cabinet médical */
  public async addPatient(patient: PatientInterface): Promise<PatientInterface> {
    const res = await this._http.post('/addPatient', {
      patientName: patient.nom,
      patientForname: patient.prenom,
      patientNumber: patient.numeroSecuriteSociale,
      patientSex: patient.sexe === sexeEnum.M ? 'M' : 'F',
      patientBirthday: 'AAAA-MM-JJ',
      patientFloor: patient.adresse.etage,
      patientStreetNumber: patient.adresse.numero,
      patientStreet: patient.adresse.rue,
      patientPostalCode: patient.adresse.codePostal,
      patientCity: patient.adresse.ville
    }, {observe: 'response'}).toPromise<HttpResponse<any>>();

    if (res.status === 200) {
      // OK on peut ajouter en local
        return patient;
    }
    return null;
  }

  /** @desc Promet d'affecter l'infirmier ayant pour id  'infirmierId' au patient ayant pour id 'patientId" */
  public async affectationPatient(infirmierId: string , patientId: string): Promise<number> {
    const res = await this._http.post( '/affectation', {
      infirmier: infirmierId ,
      patient: patientId
    }, {observe: 'response'}).toPromise<HttpResponse<any>>();

    if (res.status === 200) {
      // OK on peut ajouter en local
      return 1;
    }
    return null;
  }

}
