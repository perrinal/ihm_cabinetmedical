import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { SecretaryComponent } from './secretary/secretary.component';
import {HttpClientModule} from '@angular/common/http';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {
  MatButtonModule, MatCard, MatCardModule,
  MatExpansionModule,
  MatFormFieldModule,
  MatGridListModule,
  MatInputModule,
  MatPaginatorModule,
  MatSelectModule, MatSnackBarModule,
  MatSortModule,
  MatTabsModule,
  MatTooltipModule,
  ShowOnDirtyErrorStateMatcher
} from '@angular/material';
import {MatToolbarModule} from '@angular/material/toolbar';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {MatMenuModule} from '@angular/material/menu';
import { InfirmiersComponent } from './infirmiers/infirmiers.component';
import { PatientsComponent } from './patients/patients.component';
import { HomeComponent } from './home/home.component';
import {MatTableModule} from '@angular/material/table';
import { DialogueFormPatientComponent } from './dialogue-form-patient/dialogue-form-patient.component';
import {MatDialogModule} from '@angular/material';
import {MatRadioModule} from '@angular/material';
import {ErrorStateMatcher} from '@angular/material/core';
import { DialogueAffectationPatientComponent } from './dialogue-affectation-patient/dialogue-affectation-patient.component';
import {DragDropModule} from '@angular/cdk/drag-drop';

@NgModule({
  declarations: [
    AppComponent,
    SecretaryComponent,
    InfirmiersComponent,
    PatientsComponent,
    HomeComponent,
    DialogueFormPatientComponent,
    DialogueAffectationPatientComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    BrowserAnimationsModule,
    MatToolbarModule,
    MatButtonModule,
    MatGridListModule,
    FormsModule,
    MatMenuModule,
    MatTableModule,
    MatDialogModule,
    MatFormFieldModule,
    MatRadioModule,
    MatInputModule,
    ReactiveFormsModule,
    MatSortModule,
    MatPaginatorModule,
    MatTabsModule,
    DragDropModule,
    MatSelectModule,
    MatExpansionModule,
    MatTooltipModule,
    MatSnackBarModule,
    MatCardModule

  ],
  providers: [
    {provide: ErrorStateMatcher, useClass: ShowOnDirtyErrorStateMatcher}
  ],
  bootstrap: [AppComponent],
  entryComponents: [DialogueFormPatientComponent, DialogueAffectationPatientComponent]

})
export class AppModule { }
