import {Component, Inject, OnInit} from '@angular/core';
import {ErrorStateMatcher, MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {FormControl, FormGroupDirective, NgForm, Validators} from '@angular/forms';
import {PatientInterface} from '../dataInterfaces/patient';
import {CabinetInterface} from '../dataInterfaces/cabinet';
import {CabinetMedicalService} from '../cabinet-medical.service';
import {sexeEnum} from '../dataInterfaces/sexe';


/** Le formulaire est toujours faux :  si le n° de secu est deja attribué */
export class MyErrorStateMatcher1 implements ErrorStateMatcher {
  isErrorState(control: FormControl | null, form: FormGroupDirective | NgForm | null): boolean {
    return true;
  }
}

/** Le formulaire est faux si on nespecte pas son FormControl, si il est vide , ... */
export class MyErrorStateMatcher2 implements ErrorStateMatcher {
  isErrorState(control: FormControl | null, form: FormGroupDirective | NgForm | null): boolean {
    const isSubmitted = form && form.submitted;
    return !!(control && control.invalid && (control.dirty || control.touched || isSubmitted));  }
}


@Component({
  selector: 'app-dialogue-form-patient',
  templateUrl: './dialogue-form-patient.component.html',
  styleUrls: ['./dialogue-form-patient.component.scss']
})
export class DialogueFormPatientComponent implements OnInit {

  /** data binding */
  sexe: string;

  /** FormControl pour chaque champs du formulaire : regex */
  nomControl = new FormControl(this.data ? this.data.nom : '',
    [Validators.required, Validators.pattern('[A-Z]?([a-z]+[éèàêë]?)+([-][A-Z]?([a-z]+[éèàêë]?)+)?')]);
  prenomControl = new FormControl(this.data ? this.data.prenom : '',
    [Validators.required, Validators.pattern('[A-Z]?([a-z]+[éèàêë]?)+([-][A-Z]?([a-z]+[éèàêë]?)+)?')]);
  numeroControl = new FormControl(this.data ? this.data.adresse.numero : '',
    [Validators.required, Validators.pattern('\\d*')]);
  codePostalControl = new FormControl(this.data ? this.data.adresse.codePostal : '',
    [Validators.required, Validators.pattern('\\d{5}')]);
  rueControl = new FormControl(this.data ? this.data.adresse.rue : '',
    [Validators.required, Validators
      .pattern('[A-Z](([a-z]*[éèàêë]?)*(([\\s]|[-])([a-z]\')?[A-Z](([a-z]*[éèàêë]?)*))*)')]);
  villeControl = new FormControl(this.data ? this.data.adresse.ville : '',
    [Validators.required, Validators.pattern('[A-Z](([a-z]*[éèàêë]?)*(([\\s]|[-])([a-z]\')?[A-Z](([a-z]*[éèàêë]?)*))*)')]);
  etageControl = new FormControl(this.data ? this.data.adresse.etage : '',
    [Validators.required, Validators.pattern('\\d{1,2}')]);
  numeroSecuriteSocialControl = new FormControl('',
    [Validators.required, Validators.pattern('[1-4]\\d\\d((0[1-9])|(1[0-2]))\\d[0-9AB]\\d{8}')]);

  /** Attribué aux formulaire pour saisir le n° de secu, si le n° existe on switch avec matcher1 sinon matcher2*/
  matcher1 = new MyErrorStateMatcher1();
  matcher2 = new MyErrorStateMatcher2();

  private _cms: CabinetInterface;
  private _patients: PatientInterface[];

  public get cms(): CabinetInterface { return this._cms; }
  public get patients(): PatientInterface[] { return this._patients; }

  constructor(public cabinetMedicalService: CabinetMedicalService, public dialogRef: MatDialogRef<DialogueFormPatientComponent>,
              @Inject(MAT_DIALOG_DATA) public data: PatientInterface | null) {
    this.initCabinet(cabinetMedicalService);
  }

  async initCabinet(cabinetMedicalService) {
    this._cms = await cabinetMedicalService.getData('/data/cabinetInfirmier.xml');
    this._patients = cabinetMedicalService.getPatients(this.cms);
  }


  /** @desc Ferme le dialogue */
  onNoClick(): void {
    this.dialogRef.close();
  }

  ngOnInit(): void {
  }

  /** @desc Retourne le patient a créer */
  createPatient(nom: string, prenom: string, numero: string, rue: string, codePostal: string,
                etage: string, ville: string, numeroSecuriteSocial: string) {
    return {prenom: prenom,
            nom: nom,
            sexe: this.sexe === '0' ? 0 : 1,
            numeroSecuriteSociale: numeroSecuriteSocial,
            adresse: {
              ville: ville,
              codePostal: codePostal,
              rue: rue,
              numero: numero,
              etage: etage
            }};
  }

  /** @desc Retourne le patient a update */
  updatePatient(nom: string, prenom: string, numero: string, rue: string, codePostal: string,
                etage: string, ville: string) {
    return {prenom: prenom,
      nom: nom,
      sexe: this.data.sexe,
      numeroSecuriteSociale: this.data.numeroSecuriteSociale,
      adresse: {
        ville: ville,
        codePostal: codePostal,
        rue: rue,
        numero: numero,
        etage: etage
      }};
  }

  /** @desc En fonction du formulaire et de son formControl, on retourne un message d'erreur spécifique */
  getErrorMessage(champ: string) {
    switch (champ) {
      case 'nom':
        return this.nomControl.hasError('required') ? '*champ incomplet' :
          this.nomControl.hasError('pattern') ? '*caractères alphabétiques' :
            '';
      case 'prenom':
        return this.prenomControl.hasError('required') ? '*champ incomplet' :
          this.prenomControl.hasError('pattern') ? '*caractères alphabétiques' :
            '';
      case 'numero':
        return this.numeroControl.hasError('required') ? '*champ incomplet' :
          this.numeroControl.hasError('pattern') ? '*caractères numériques' :
            '';
      case 'codePostal':
        return this.codePostalControl.hasError('required') ? '*champ incomplet' :
          this.codePostalControl.hasError('pattern') ? '*caractères numériques de taille au format XXXXX' :
            '';
      case 'rue':
        return this.rueControl.hasError('required') ? '*champ incomplet' :
          this.rueControl.hasError('pattern') ? '*caractères alphabétiques et majascule au début de chaque mot' :
            '';
      case 'ville':
        return this.villeControl.hasError('required') ? '*champ incomplet' :
          this.villeControl.hasError('pattern') ? '*caractères alphabétiques et majascule au début de chaque mot' :
            '';
      case 'etage':
        return this.etageControl.hasError('required') ? '*champ incomplet' :
          this.etageControl.hasError('pattern') ? '*caractères numériques de 0 a 999' :
            '';
      case 'numeroSecuriteSocial':
        return this.numeroSecuriteSocialControl.hasError('required') ? '*champ incomplet' :
          this.numeroSecuriteSocialControl.hasError('pattern') ? '*mauvais format' :
            '';
      case 'numeroSecuriteSocial: exist':
        return '*numéro déjà attribué à un patient';

      default :
        console.log('Error MSG');
    }

  }

  /** @desc True si le n° secu est deja attribué, sinon False */
  isExistNumeroSecuriteSociale(numeroSecuriteSociale: string): boolean {
    if ( this._patients.filter( (p) => p.numeroSecuriteSociale === numeroSecuriteSociale).length !== 0 ) {
      return true;
    }
    return false;
  }


  }
