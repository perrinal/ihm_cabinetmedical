import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DialogueFormPatientComponent } from './dialogue-form-patient.component';

describe('DialogueFormPatientComponent', () => {
  let component: DialogueFormPatientComponent;
  let fixture: ComponentFixture<DialogueFormPatientComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DialogueFormPatientComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DialogueFormPatientComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
