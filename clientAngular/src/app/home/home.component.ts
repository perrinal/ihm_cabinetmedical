import { Component, OnInit } from '@angular/core';
import {MatDialog, MatSnackBar, MatCard} from '@angular/material';
import {CabinetInterface} from '../dataInterfaces/cabinet';
import {CabinetMedicalService} from '../cabinet-medical.service';
import {Adresse} from '../dataInterfaces/adresse';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  private _cms: CabinetInterface;
  _nbInfirmiers: number;
  _nbPatients: number;
  _adresse: string;
  retour: string;

  public get cms(): CabinetInterface { return this._cms; }
  public get nbInfirmiers(): number { return this._nbInfirmiers; }
  public get nbPatients(): number {return this._nbPatients; }
  public get adresse(): string {return this._adresse; }
  public get nbNonAffecte(): number { return this._cms.patientsNonAffectés.length; }
  public get nbAffecte(): number { return this.nbPatients - this.nbNonAffecte; }

  constructor(public cabinetMedicalService: CabinetMedicalService, public dialog: MatDialog, public snackBar: MatSnackBar ) {
    this.initCabinet(cabinetMedicalService);
  }


  async initCabinet(cabinetMedicalService) {
    this._cms = await cabinetMedicalService.getData('/data/cabinetInfirmier.xml');
    this._nbInfirmiers = this.cms.infirmiers.length;
    this._nbPatients = cabinetMedicalService.getPatients(this.cms).length;
    this.getAdresse(this.cms.adresse);
  }

  getAdresse(adr: Adresse) {
    this._adresse = 'l\'étage ' + adr.etage + ', au ' + adr.numero + ' ' + adr.rue + ' ' + adr.codePostal + ' ' + adr.ville ;
  }

  getAffecNonAffec(affec: number, nonAffec: number){
    let retour: string;
    if (affec === 1) {
      retour = '1 seul patient est affecté';
    } else {
      retour = affec + ' patients sont affectés';
    }
    if (nonAffec === 1){
      retour = retour + ' et 1 patient n\'est pas affecté.';
    } else {
      retour = retour + ' et ' + nonAffec + ' patients ne le sont pas.';
    }
    return retour;
  }

  getInfPatients(inf: number, patients: number){
    let retour: string;
    if (inf === 1) {
      retour = 'Le cabinet possède 1 seul infirmier';
    } else {
      retour = 'Le cabinet possède ' + inf + ' infirmiers';
    }
    if (patients === 1){
      retour = retour + ' et 1 seul patient.';
    } else {
      retour = retour + ' et ' + patients + ' patients.';
    }
    return retour;
  }



  ngOnInit() {
  }

}
