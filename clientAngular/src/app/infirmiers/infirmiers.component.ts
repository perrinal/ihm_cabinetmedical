import {Component, OnInit, ViewChild} from '@angular/core';
import {CabinetMedicalService} from '../cabinet-medical.service';
import {MatDialog, MatPaginator, MatSnackBar, MatSort, MatSortable, MatTableDataSource} from '@angular/material';
import {CabinetInterface} from '../dataInterfaces/cabinet';
import {PatientInterface} from '../dataInterfaces/patient';
import {Adresse} from '../dataInterfaces/adresse';
import {InfirmierInterface} from '../dataInterfaces/infirmier';



// @ts-ignore
@Component({
  selector: 'app-infirmiers',
  templateUrl: './infirmiers.component.html',
  styleUrls: ['./infirmiers.component.scss']
})
export class InfirmiersComponent implements OnInit {

  displayedColumnsPatients: string[] = ['nom', 'prenom', 'sexe', 'adresse', 'numeroSecuriteSociale', 'desaffecte'];
  private _cms: CabinetInterface;
  private _patients: PatientInterface[];
  private _infirmiers: InfirmierInterface[];
  private _id: string;

  public get cms(): CabinetInterface { return this._cms; }
  public get patients(): PatientInterface[] { return this._patients; }
  public get id(): string {return this._id; }

  constructor(public cabinetMedicalService: CabinetMedicalService, public dialog: MatDialog, public snackBar: MatSnackBar ) {
    this.initCabinet(cabinetMedicalService);
  }

  /** @desc Initialise le cabinent médical '_cms' */
  async initCabinet(cabinetMedicalService) {
    this._cms = await cabinetMedicalService.getData('/data/cabinetInfirmier.xml');
    this._patients =  [...cabinetMedicalService.getPatients(this.cms)];

  }

  getAdresse(adr: Adresse) {
    if (adr.etage === '') {
      return adr.numero + ' ' + adr.rue + ' ' + adr.codePostal + ' ' + adr.ville ;
    } else {
      return 'étage ' + adr.etage + ', au ' + adr.numero + ' ' + adr.rue + ' ' + adr.codePostal + ' ' + adr.ville ;
    }
  }

  getInfirmier(id: string) {
   for (let i = 0; i < this.cms.infirmiers.length; i++) {
     if (this.cms.infirmiers[i].id === id) {
       return this.cms.infirmiers[i].patients;
     }
   }

   }

desaffecter(patient: PatientInterface, id: string) {
     this.cms.patientsNonAffectés.push(patient);
     for (let i = 0; i < this.cms.infirmiers.length; i++) {
       if (this.cms.infirmiers[i].id === id) {
         this.cms.infirmiers[i].patients = this.cms.infirmiers[i].patients.filter((P) => P.numeroSecuriteSociale !== patient[0].numeroSecuriteSociale);
       }
     }
      this.cabinetMedicalService.affectationPatient('none', patient[0].numeroSecuriteSociale);

   }

  /** @desc Bar qui s'affiche en bas de l'ecran lors d'une action du type : ajouter , update , affecter patient */
  openSnackBar(message: string, action: string) {
    this.snackBar.open(message, action, {
      duration: 5000,
    });
  }

  ngOnInit() {
  }

}
