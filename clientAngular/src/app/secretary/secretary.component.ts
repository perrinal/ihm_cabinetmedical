import { Component, OnInit } from '@angular/core';
import {CabinetInterface} from '../dataInterfaces/cabinet';
import {CabinetMedicalService} from '../cabinet-medical.service';

@Component({
  selector: 'app-secretary',
  templateUrl: './secretary.component.html',
  styleUrls: ['./secretary.component.scss']
})

export class SecretaryComponent implements OnInit {

  /** _cms ; CabinetInterface <- lecture des données de cabinetInfirmier.xml **/
  private _cms: CabinetInterface;
  public get cms(): CabinetInterface { return this._cms; }

  constructor(cabinetMedicalService: CabinetMedicalService) {
     this.initCabinet(cabinetMedicalService);
  }

  async initCabinet(cabinetMedicalService) {
    this._cms = await cabinetMedicalService.getData('/data/cabinetInfirmier.xml');
  }

  ngOnInit() {
  }

}
