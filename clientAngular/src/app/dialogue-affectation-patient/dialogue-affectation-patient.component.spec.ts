import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DialogueAffectationPatientComponent } from './dialogue-affectation-patient.component';

describe('DialogueAffectationPatientComponent', () => {
  let component: DialogueAffectationPatientComponent;
  let fixture: ComponentFixture<DialogueAffectationPatientComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DialogueAffectationPatientComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DialogueAffectationPatientComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
