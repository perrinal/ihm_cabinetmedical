import {Component, Inject, OnInit} from '@angular/core';
import {CabinetInterface} from '../dataInterfaces/cabinet';
import {PatientInterface} from '../dataInterfaces/patient';
import {CabinetMedicalService} from '../cabinet-medical.service';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {DialogueFormPatientComponent} from '../dialogue-form-patient/dialogue-form-patient.component';
import {CdkDragDrop, CdkDragEnter, moveItemInArray, transferArrayItem} from '@angular/cdk/drag-drop';
import {InfirmierInterface} from '../dataInterfaces/infirmier';

@Component({
  selector: 'app-dialogue-affectation-patient',
  templateUrl: './dialogue-affectation-patient.component.html',
  styleUrls: ['./dialogue-affectation-patient.component.scss']
})
export class DialogueAffectationPatientComponent implements OnInit {

  private _cms: CabinetInterface;
  private _patients: PatientInterface[];
  private _patientsNonAffectes: PatientInterface[];
  private _patientsAffectes: Map<string, PatientInterface[]> = new Map();
  private _patientsAffectesNew: Map<string, PatientInterface[]> = new Map();

  public get cms(): CabinetInterface { return this._cms; }
  public get patients(): PatientInterface[] { return this._patients; }
  public get patientsNonAffectes(): PatientInterface[] { return this._patientsNonAffectes; }
  public get patientsAffectes():  Map<string, PatientInterface[]> { return this._patientsAffectes; }
  public get patientsAffectesNew():  Map<string, PatientInterface[]> { return this._patientsAffectesNew; }

  constructor(public cabinetMedicalService: CabinetMedicalService, public dialogRef: MatDialogRef<DialogueFormPatientComponent>) {
    this.initCabinet(cabinetMedicalService);
  }

  async initCabinet(cabinetMedicalService) {
    this._cms = await cabinetMedicalService.getData('/data/cabinetInfirmier.xml');
    this._patients = cabinetMedicalService.getPatients(this.cms);
    this._patientsNonAffectes = [...this._cms.patientsNonAffectés];
    this._cms.infirmiers.map( (I) => this._patientsAffectes.set(I.id, [...I.patients]));

  }


  onNoClick(): void {
    this.dialogRef.close();
  }

  ngOnInit(): void {
  }

  /** @desc Gestion du drag drop : copier du site angular material */
  drop(event: CdkDragDrop<string[]>) {
    if (event.previousContainer === event.container) {
      moveItemInArray(event.container.data, event.previousIndex, event.currentIndex);
    } else {
      transferArrayItem(event.previousContainer.data,
        event.container.data,
        event.previousIndex,
        event.currentIndex);
    }
  }

  /** @desc Affectation ou desaffectation de tout les patients qui on changé d'affectation */
  async assignedAllPatientsMove() {
    /** Désaffections des patients */
    const pp: PatientInterface[] =
    this.patientsNonAffectes.
    filter( (P) =>
      (this._cms.patientsNonAffectés.filter((p) => p.numeroSecuriteSociale === P.numeroSecuriteSociale )).length === 0);



    let pas;
    for (pas = 0; pas < pp.length; pas++) {
      /** Obligé d'attendre la résolution de la requete.
      /* Si je fais this.pp.forEach ( (P) => this.affectationPatient(P, 'none'); ,
      /* alors lorsqu'il y a un grand nombre de patient a desacffecter le serveur plante :
      /* Erreurs : - HTTPReponse error 502
      /*           - ERROR Error: "Uncaught (in promise): TypeError: root is null... ( cabinetInfirmier.xml devient mal formé )
      /* Cette erreur apparait seulement lorsque l'on veut désaffecter un grand nombre de patient.
      /* Si l'on veut affecter un grand nombre de patient il n'y a aucun soucis.
      /* Ce problème implique de privilégier le synchrone au lieu de l'asynchrone **/
       await this.affectationPatient(pp[pas], 'none');
    }

    /** Affectations patients */
    this._cms.infirmiers.map( (I) => this._patientsAffectesNew.set(I.id, this.patientsAffectes.get(I.id)
        .filter( (P) => (I.patients.filter((p) => p.numeroSecuriteSociale === P.numeroSecuriteSociale)).length === 0)));




    this._patientsAffectesNew.forEach( (v, k) => v.map((P) => this.affectationPatient(P, k)));

    this.dialogRef.close();
  }

  /** @desc Affectation ou desaffectation d'un patient a un infirmier */
  async affectationPatient(patient: PatientInterface, infirmierId: string) {
    let requestIsCorrect;

    requestIsCorrect = await this.cabinetMedicalService.affectationPatient(infirmierId, patient.numeroSecuriteSociale);


    if (requestIsCorrect != null) {
      console.log('AFFECTATION REQUEST : OK', requestIsCorrect);
    } else {
      console.log('AFFECTATION REQUEST : KO', requestIsCorrect);
    }
  }









}
