import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {SecretaryComponent} from './secretary/secretary.component';
import {InfirmiersComponent} from './infirmiers/infirmiers.component';
import {PatientsComponent} from './patients/patients.component';
import {HomeComponent} from './home/home.component';

const routes: Routes = [
  { path: '', redirectTo: '/home', pathMatch: 'full' },
  { path: 'infirmiers', component: InfirmiersComponent },
  { path: 'patients', component: PatientsComponent },
  { path: 'home', component: HomeComponent },
  { path: 'secretary', component: SecretaryComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
